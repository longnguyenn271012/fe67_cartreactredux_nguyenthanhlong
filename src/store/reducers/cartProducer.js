const initialState = {
  cart: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_TO_CART": {
      const cloneCart = [...state.cart];
      const foundIndex = cloneCart.findIndex((item) => {
        return item.prod.id === action.payload.id;
      });
      if (foundIndex === -1) {
        const cartItem = { prod: action.payload, quantity: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[foundIndex].quantity++;
      }
      state.cart = cloneCart;
      return { ...state };
    }

    case "DELETE_CART_ITEM": {
      const cloneCart = [...state.cart];
      const foundIndex = cloneCart.findIndex((item) => {
        return item.prod.id === action.payload;
      });
      if (foundIndex === -1) return;
      cloneCart.splice(foundIndex, 1);
      state.cart = cloneCart;
      return { ...state };
    }

    case "INCREASE_QUANTITY": {
      const cloneCart = [...state.cart];
      const foundIndex = cloneCart.findIndex((item) => {
        return item.prod.id === action.payload;
      });
      if (foundIndex === -1) return;
      cloneCart[foundIndex].quantity++;
      state.cart = cloneCart;
      return { ...state };
    }

    case "DECREASE_QUANTITY": {
      const cloneCart = [...state.cart];
      const foundIndex = cloneCart.findIndex((item) => {
        return item.prod.id === action.payload;
      });
      if (foundIndex === -1) return;
      if (cloneCart[foundIndex].quantity === 1) cloneCart.splice(foundIndex, 1);
      else cloneCart[foundIndex].quantity--;
      state.cart = cloneCart;
      return { ...state };
    }

    default:
      return state;
  }
};

export default cartReducer;
