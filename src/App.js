import logo from './logo.svg';
import './App.css';
import Home from './Cart/Home';

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
