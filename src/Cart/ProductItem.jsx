import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../store/action";

class ProductItem extends Component {
  handleView = (id) => () => {
    this.props.dispatch(createAction("SET_SELECTED_PRODUCT", id));
  };

  handleAddToCart = (product) => () => {
    this.props.dispatch(createAction("ADD_TO_CART", product));
  };

  render() {
    const { img, name, id } = this.props.product;
    return (
      <div className="card mx-3">
        <img src={img} style={{ height: 300, width: "100%" }}></img>
        <div className="card-body">
          <h4>{name}</h4>
          <button className="btn btn-info mr-3" onClick={this.handleView(id)}>
            Chi tiết
          </button>
          <button
            className="btn btn-danger"
            onClick={this.handleAddToCart(this.props.product)}
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(ProductItem);
