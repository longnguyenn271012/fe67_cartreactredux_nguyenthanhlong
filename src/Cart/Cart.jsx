import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../store/action";

class Cart extends Component {
  handleDeleteCartItem = (id) => () => {
    this.props.dispatch(createAction("DELETE_CART_ITEM", id));
  };

  handleIncreaseQuantity = (id) => () => {
    this.props.dispatch(createAction("INCREASE_QUANTITY", id));
  };

  handleDecreaseQuantity = (id) => () => {
    this.props.dispatch(createAction("DECREASE_QUANTITY", id));
  };

  renderCart = () => {
    const cartHTML = this.props.cart.map((item) => {
      const { id, name, img, price } = item.prod;
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>{name}</td>
          <td>
            <img style={{ width: 100 }} src={img} alt="product" />
          </td>
          <td>
            <button
              className="btn btn-info"
              onClick={this.handleDecreaseQuantity(id)}
            >
              -
            </button>
            <span>{item.quantity}</span>
            <button
              onClick={this.handleIncreaseQuantity(id)}
              className="btn btn-info"
            >
              +
            </button>
          </td>
          <td>{price}</td>
          <td>{price * item.quantity}</td>
          <td>
            <button
              onClick={this.handleDeleteCartItem(id)}
              className="btn btn-success"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
    return cartHTML;
  };

  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-xl" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Giỏ Hàng</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th>Mã sản phẩm</th>
                    <th>Tên sản phẩm</th>
                    <th>Hình ảnh</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th>Thành tiền</th>
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>{this.renderCart()}</tbody>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.productReducer.products,
    cart: state.cartReducer.cart,
  };
};

export default connect(mapStateToProps)(Cart);
