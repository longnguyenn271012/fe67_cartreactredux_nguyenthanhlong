import React, { Component } from 'react';
import ProductItem from './ProductItem';
import { connect } from 'react-redux'

class ProductList extends Component {
    renderProucts = () => {
        return this.props.products.map((item) => {
            return(
                <div key={item.id} className="col-3">
                    <ProductItem product={item} selectedProduct={this.props.selectedProduct} />
                </div>
            )
        })
    }

    render() {
        return (
            <div className="container-fluit">
                <div className="row">
                    {this.renderProucts()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.productReducer.products,
        selectedProduct: state.productReducer.selectedProduct,
    };
}

export default connect(mapStateToProps)(ProductList);