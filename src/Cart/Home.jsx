import React, { Component } from "react";
import ProductList from "./ProductList";
import Detail from "./Detail";
import Cart from "./Cart";

class Home extends Component {
  render() {
    return (
      <div>
        <h1>Bài Tập Giỏ Hàng</h1>
        <p data-toggle="modal" data-target="#modelId" className="text-center">
          Giỏ Hàng
        </p>
        <ProductList />
        <Detail />
        <Cart />
      </div>
    );
  }
}


export default Home;
