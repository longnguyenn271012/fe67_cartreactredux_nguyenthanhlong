import React, { Component } from 'react';
import { connect } from 'react-redux';

class Detail extends Component {
  renderDetail = () => {
    return this.props.products
      .filter((item) => { return item.id === this.props.selectedProduct })
      .map((item) => {
        const { name, screen, backCamera, frontCamera, img, desc } = item;
        return (
          <div className="row">
            <div className="col-4">
              <h4>{name}</h4>
              <img src={img} alt="product" className="w-100" />
            </div>
            <div className="col-8">
              <h4>Thông số kĩ thuật</h4>
              <table className="table">
                <tr>
                  <td>Màn hình</td>
                  <td>{screen}</td>
                </tr>
                <tr>
                  <td>Camera Trước</td>
                  <td>{frontCamera}</td>
                </tr>
                <tr>
                  <td>Camera sau</td>
                  <td>{backCamera}</td>
                </tr>
                <tr>
                  <td>Mô tả</td>
                  <td>{desc}</td>
                </tr>
              </table>
            </div>
          </div>
        );
      });
  };

    render() {
        return (
          <div className="container mt-5">
            {this.renderDetail()}
          </div>
          );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.productReducer.products,
        selectedProduct: state.productReducer.selectedProduct,
    }
}

export default connect(mapStateToProps)(Detail);